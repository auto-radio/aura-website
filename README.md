# Aura Website

This repository holds the [aura.radio](https://aura.radio) static landing page.

The website utilizes [Tailwind CSS](https://tailwindcss.com/) and [DaisyUI](https://daisyui.com/).

## Requirements

- Node 18+
- git

## Installation

```
npm install
```

## Development

Run the development server:

```
npm run dev
```

You can access the development server via `http://localhost:3000/`.

Note that live-reloading is not provided. You have to refresh your browser after changes.

## Deployment

The website is automatically build and deployed by Gitlab CI.

## License

- Logos and trademarks of sponsors and supporters are copyrighted by the respective owners. They should be removed if you fork this repository.
- All source code is licensed under [GNU Affero General Public License (AGPL) v3.0](https://www.gnu.org/licenses/agpl-3.0.en.html).
- All other assets and text are licensed under [Creative Commons BY-NC-SA v3.0](https://creativecommons.org/licenses/by-nc-sa/3.0/).

These licenses apply unless stated differently.